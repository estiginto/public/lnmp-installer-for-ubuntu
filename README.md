# Linux Nginx MySQL PHP installer for Ubuntu

will install following for developing Laravel :)

* Nginx
* PHP 7.4, 8.0, 8.1, 8.2
* Mariadb services script (by docker)
* Adminer services script (by docker, mysql web tool)
* Redis services script (by docker)
* Mailhog services script (by docker)


# install or upgrade

```
curl -s https://gitlab.com/estiginto/public/lnmp-installer-for-ubuntu/-/raw/main/install.sh | bash
```

# how to use

啟動 workspace 後建立專案 (此次使用新專案示範 可以 clone 後自行初始化)
```
composer create-project laravel/laravel example-app
```

設定目前專案目錄並重啟服務 (可使用絕對路徑也可以使用相對路徑)
```
chproject /home/workspace/example-app
lnmp-restart #重啟服務套用目錄
```

# other tools

切換 php 版本
```
chphp (8.1|7.4) #切換 php 版本
lnmp-restart #重啟服務套用版本
```
```

# Dev URLs 說明
* 80 專案 http
* 443 專案 https
* 8080 adminer (MySQL 管理)(預設帳/密 root/password)
