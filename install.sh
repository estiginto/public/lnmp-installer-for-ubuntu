#!/bin/bash

# config
declare -a php_versions=("7.4" "8.0" "8.1" "8.2")
user=$(whoami)
group=$(id -gn)

archive_link_etc=https://gitlab.com/estiginto/public/lnmp-installer-for-ubuntu/-/archive/main/lnmp-installer-for-ubuntu-main.tar.gz?path=etc
archive_link_scripts=https://gitlab.com/estiginto/public/lnmp-installer-for-ubuntu/-/archive/main/lnmp-installer-for-ubuntu-main.tar.gz?path=scripts

SCRIPTS_PATH=/usr/local/scripts

# apt-get update unattended
DEBIAN_FRONTEND=noninteractive

# validate password once
sudo -v
while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &

# install software-properties-common (get add-apt-repository)
echo ===========================================
echo install software-properties-common
echo ===========================================
sudo apt-get update -qq && sudo apt-get install -qqy software-properties-common

# add repo
echo ===========================================
echo add repo for php \& nginx
echo ===========================================
sudo add-apt-repository ppa:ondrej/php -ny > /dev/null
sudo add-apt-repository ppa:ondrej/nginx -ny > /dev/null

# install necessary tools
echo ===========================================
echo install necessary tools
echo ===========================================
sudo apt-get update -qq && sudo apt-get install -qqy \
    build-essential \
    procps \
    file \
    ca-certificates \
    curl \
    gnupg \
    lsb-release \
    git \
    wget \
    vim \
    sudo \
    screen \
    htop \
    bash-completion \
    unzip

# install services
echo ===========================================
echo install supervisor and nginx
echo ===========================================
sudo apt-get install -qqy \
    supervisor \
    nginx

# install php
for i in "${php_versions[@]}"
do
    echo ===========================================
    echo install php $i
    echo ===========================================
    sudo apt-get update -qq && sudo apt-get install -qqy \
        php$i \
        php$i-fpm \
        php$i-bcmath \
        php$i-ctype \
        php$i-curl \
        php$i-dom \
        php$i-fileinfo \
        php$i-mbstring \
        php$i-pdo \
        php$i-tokenizer \
        php$i-xml \
        php$i-mysql \
        php$i-xdebug \
        php$i-sqlite \
        php$i-zip \
        php$i-gd \
        php$i-redis \
        php$i-imagick \
        php$i-swoole \
        php$i-intl
done

# install composer
echo ===========================================
echo install php composer
echo ===========================================
wget https://getcomposer.org/installer -O - -q | sudo php -- --quiet --install-dir=/usr/local/bin/ --filename=composer
sudo chown $user:$group /usr/local/bin/composer

# install docker
echo ===========================================
echo install docker
echo ===========================================
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor --batch --yes -o /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update -qq && sudo apt-get install -qqy docker-ce docker-ce-cli containerd.io docker-compose-plugin
sudo usermod -aG docker $user

# download etc
echo ===========================================
echo download etc \& script files
echo ===========================================
wget -qc $archive_link_scripts -O - | sudo tar -xz --strip-components=1 --directory /usr/local
wget -qc $archive_link_etc -O - | sudo tar -xz --strip-components=1 --directory /


# replace user name for nginx & php-fpm config
echo ===========================================
echo replace user name for nginx \& php-fpm config
echo ===========================================
sudo sed -i "s/workspace/$user/g" /etc/nginx/nginx.conf
for i in "${php_versions[@]}"
do
    sudo sed -i "s/workspace/$user/g" /etc/php/$i/fpm/pool.d/www.conf
done

# setup scripts PATH
echo ===========================================
echo setup PATH for scripts
echo ===========================================
grep -qxF "export PATH=\"\$PATH:$SCRIPTS_PATH\"" ~/.bashrc || echo "export PATH=\"\$PATH:$SCRIPTS_PATH\"" >> ~/.bashrc

# setup project link
echo ===========================================
echo setup project link
echo ===========================================
sudo rm -f /var/www/project
sudo ln -s $HOME/.project /var/www/project

# start docker services
echo ===========================================
echo start docker services
echo ===========================================
sudo su $USER -c "/usr/local/scripts/docker-mariadb start"
sudo su $USER -c "/usr/local/scripts/docker-redis start"
sudo su $USER -c "/usr/local/scripts/docker-adminer start"
sudo su $USER -c "/usr/local/scripts/docker-mailhog start"
